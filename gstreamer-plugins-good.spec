%bcond_with extras

Name:           gstreamer-plugins-good
Version:        0.10.31
Release:        26
Summary:        Well tested and good licensing plugins for GStreamer framework
License:        LGPLv2+
URL:            http://gstreamer.freedesktop.org/
Source:         http://gstreamer.freedesktop.org/src/gst-plugins-good/gst-plugins-good-%{version}.tar.xz
Patch0001:      0001-fix-v4l2_munmap.patch
Patch0002:      0002-clear_DISCONT_flag.patch
Patch0003:      0003-v4l2src-fix.patch
Patch0004:      0004-v4l2object-Don-t-probe-UVC-devices-for-being-interla.patch
Patch0005:      0001-sys-v4l2-Some-blind-compilation-fixes.patch
Patch0006:      gnome-724085.patch
Patch0007:      Adapt-to-backwards-incompatible-change-in-GNU-Make-4.3.patch

Patch6000:      CVE-2016-9636-CVE-2016-9635-CVE-2016-9634-pre.patch
Patch6001:      CVE-2016-9636-CVE-2016-9635-CVE-2016-9634-1.patch
Patch6002:      CVE-2016-9636-CVE-2016-9635-CVE-2016-9634-2.patch
Patch6003:      CVE-2017-5840.patch
Patch6004:      CVE-2016-9810.patch
Patch6005:      CVE-2016-10199-pre.patch
Patch6006:      CVE-2016-10199.patch
Patch6007:      CVE-2016-9808-CVE-2016-9807-pre-1.patch
Patch6008:      CVE-2016-9808-CVE-2016-9807-pre-2.patch
Patch6009:      CVE-2016-9808-CVE-2016-9807.patch
Patch6010:      CVE-2016-10198.patch

Requires:       gstreamer >= 0.10.36 gstreamer-plugins-base
Requires(pre):  GConf2
Requires(preun):GConf2
Requires(post): GConf2
BuildRequires:  gstreamer-devel >= 0.10.36 gstreamer-plugins-base-devel >= 0.10.36
BuildRequires:  liboil-devel >= 0.3.6 gettext gcc-c++ cairo-devel flac-devel >= 1.1.3
BuildRequires:  GConf2-devel glibc-devel gtk2-devel kernel-headers libjpeg-devel
BuildRequires:  libpng-devel >= 1.2.0 libshout-devel libsoup-devel libX11-devel
BuildRequires:  orc-devel pulseaudio-libs-devel speex-devel taglib-devel
BuildRequires:  wavpack-devel libv4l-devel gtk-doc python3-devel chrpath
%if %{with extras}
BuildRequires:  jack-audio-connection-kit-devel
%endif
Provides:       gstreamer-plugins-pulse = 0.9.8-1 gstreamer-plugins-good-devel-docs = 0.10.31-21
Obsoletes:      gstreamer-plugins-pulse < 0.9.8 gstreamer-plugins
Obsoletes:      gstreamer-plugins-good-devel-docs < 0.10.31-21

%description
GStreamer is a pipeline-based multimedia framework that links together a wide variety
of media processing systems to complete complex workflows, based on graphs of filters
which operate on media data. GStreamer supports a wide variety of media-handling
components, such as real-time sound processing and videos playback, and about anything
else media-related. The formats and processes can be changed in plugins since its
plugin-based architecture.
GStreamer plugins "Good" represents a collection of well-supported plug-ins of good
quality and under the LGPL license.

%if %{with extras}
%package extras
Summary:        Extra GStreamer plug-ins with good code and licensing
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description extras
This package contains extra "good" plugins which are not used very much and require
additional libraries to be installed.
%endif

%prep
%autosetup -n gst-plugins-good-%{version} -p1

%build
%configure \
  --with-package-name='openEuler gstreamer-plugins-good package' \
  --with-package-origin='https://openeuler.org/en/building/download.html' \
  --enable-experimental --enable-orc --disable-monoscope \
  --disable-aalib --disable-esd --disable-libcaca --disable-flx \
%if %{with extras}
  --enable-jack \
%else
  --disable-jack \
%endif
  --with-default-visualizer=autoaudiosink
%make_build

%install
rm -rf $RPM_BUILD_ROOT
%make_install GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
%delete_la_and_a
%find_lang gst-plugins-good-0.10

chrpath -d %{buildroot}%{_libdir}/gstreamer-0.10/libgstshout2.so

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/gstreamer-0.10.schemas > /dev/null || :
fi

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/gstreamer-0.10.schemas > /dev/null || :
fi

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/gstreamer-0.10.schemas > /dev/null || :


%files -f gst-plugins-good-0.10.lang
%defattr(-, root, root)
%doc AUTHORS COPYING README REQUIREMENTS
%{_datadir}/gstreamer-0.10/
%{_libdir}/gstreamer-0.10/libgst*.so
%{_sysconfdir}/gconf/schemas/gstreamer-0.10.schemas

%if %{with extras}
%files extras
%defattr(-, root, root)
%{_libdir}/gstreamer-0.10/libgstjack.so
%endif

%changelog
* Thu Mar 18 2021 zhangtao <zhangtao221@huawei.com> - 0.10.31-26
- remove rpath

* Wed Jan 27 2021 zhanghua <zhanghua40@huawei.com> - 0.10.31-25
- fix CVE-2016-10198

* Tue Oct 27 2020 huanghaitao <huanghaitao8@huawei.com> - 0.10.31-24
- Switch to python3

* Mon Aug 03 2020 lingsheng <lingsheng@huawei.com> - 0.10.31-23
- Fix build fail with make 4.3

* Sun Jan 19 2020 fengbing <fengbing7@huawei.com> - 0.10.31-22
- Type:N/A
- ID:N/A
- SUG:N/A
- DESC:modify patch in spec file

* Thu Oct 24 2019 Alex Chao <zhaolei746@huawei.com> - 0.10.31-21
- Package init
